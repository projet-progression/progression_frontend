export default {
	name: "Menu",
	data() {
		return {
			value: new Boolean(false),
		};
	},
	props: {
		menu: null
	},
};
