export default {
	name: "NavBar",
	data() {
		return {
			nouvelUrl: "",
			exercices: new Boolean(false),
		};
	},
	emits: ["accomplissements",
		"nouvelExercice",
		"banques",
	],
	methods: {
		onUpdateRail(état){
			if(état){
				this.exercices = new Boolean(false);
			}
		}
	},
	computed: {
		menus() {
			return [
				{
					title: this.$t("menu.exercices"),
					icon: "mdi-laptop",
					value: this.exercices,
					sous_menus: [
						{title: this.$t("menu.nouveau"),
						 icon: "mdi-plus",
						 value: "nouveau",
						 action: () => this.$emit("nouvelExercice") },
						{title: this.$t("menu.accomplissement"),
						 icon: "mdi-trophy",
						 value: "accomplissements",
						 action: () => this.$emit("accomplissements") },
						 {title: this.$t("menu.banques"),
						 icon: "mdi-trophy",
						 value: "banques",
						 action: () => this.$emit("banques") },
					]
				},
			];
		}
	},
};
