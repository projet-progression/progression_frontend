import "codemirror/addon/fold/brace-fold";
import "codemirror/addon/fold/foldgutter";
import "codemirror/addon/fold/foldgutter.css";
import "codemirror/mode/clike/clike";
import "codemirror/mode/shell/shell";
import "codemirror/mode/python/python";
import "codemirror/mode/javascript/javascript";
import "codemirror/mode/rust/rust";
import "codemirror/mode/sql/sql";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/monokai.css";
import Codemirror from "codemirror-editor-vue3";
import parseMD from "@/util/parse";
import { zones } from "./zones.js";

export default {
	name: "EditeurCode",
	components: {
		Codemirror,
	},
	emits: [
		"commentaire",
	],
	data() {
		return {
			indicateurSauvegardeEnCours: false,
			indicateurModifié: false,
			sauvegardeAutomatique: null,
			zonesTraitées: false,
			cm: null,
		};
	},
	watch: {
		xray() {
			if (!this.xray) {
				this.traiterZones();
			} else {
				this.cm.setValue(this.cm.getValue());
			}
		},
		tentative() {
			this.zonesTraitées = false;
			this.changerTentative();
		},
		n_commentaires() {
			this.afficherIndicateurs();
		},
		ligneCommentairesAffichés(new_value, old_value) {
			this.marquerLignesCommentées( old_value, new_value );
		}
	},
	computed: {
		xray() {
			return this.$store.getters.xray && this.$store.getters.indicateursDeFonctionnalité("tout_voir");
		},
		raccourcis() {
			return this.$store.getters.raccourcis;
		},
		cmOptions() {
			return {
				mode: this.mode,
				theme: this.thème,
				lineNumbers: true,
				indentUnit: 4,
				extraKeys: { Tab: "indentAuto" },
				foldGutter: true,
				gutters: ["breakpoints", "CodeMirror-linenumbers", "CodeMirror-foldgutter"],
				smartIndent: false,
				font: "monospace",
			};
		},
		thème() {
			return this.$store.getters.thèmeSombre ? "monokai" : "default";
		},
		ebauches() {
			return this.$store.state.question.ebauches ?? [];
		},
		mode() {
			const value = this.$store.state.tentative.langage.toLowerCase();

			if (value === "java") {
				return "mode", "text/x-java";
			} else if (value === "javascript") {
				return "mode", "javascript";
			} else if (value === "kotlin") {
				return "mode", "text/x-kotlin";
			} else if (value === "typescript") {
				return "mode", "text/typescript";
			} else if (value === "python") {
				return "mode", "python";
			} else if (value === "bash") {
				return "mode", "shell";
			} else if (value === "rust") {
				return "mode", "text/rust";
			} else if (value === "c") {
				return "mode", "text/x-csrc";
			} else if (value === "c#") {
				return "mode", "text/x-csharp";
			} else if (value === "sql") {
				return "mode", "text/x-sql";
			} else if (["cpp", "c++"].includes(value)) {
				return "mode", "text/x-c++src";
			} else {
				return "mode", value;
			}
		},
		icone_sauvegarde() {
			return this.indicateurSauvegardeEnCours ? "mdi-pencil-outline" : this.indicateurModifié ? "mdi-pencil" : "";
		},
		tentative() {
			let tentative = this.$store.state.tentative;

			return tentative
				? new Proxy(tentative, {
					get: function (obj, prop) {
						return prop == "feedback" ? parseMD(obj[prop]) : obj[prop];
					},
				})
				: null;
		},
		tentative_réussie() {
			return this.$store.state.tentative.réussi;
		},
		testsRéussisPct() {
			return (this.$store.state.tentative.tests_réussis / this.$store.state.question.tests.length) * 100;
		},
		sauvegardeActivée() {
			return !this.$store.state.tokenRessources || this.$store.getters.username == this.$store.state.tokenRessources.username;

		},
		commentaires() {
			return this.$store.state.tentative.commentaires ?? [];
		},
		n_commentaires() {
			return this.$store.state?.tentative?.commentaires?.length ?? 0;
		},
		ligneCommentairesAffichés(){
			return this.$store.getters.ligneCommentairesAffichés;
		}
	},
	created() {
		window.onbeforeunload = this.beforeWindowUnload;
	},
	beforeUnmount() {
		this.sauvegarder();
		window.removeEventListener("beforeunload", this.beforeWindowUnload);
	},
	methods: {
		onReady(cm) {
			cm.doc.setValue(this.$store.getters.tentative?.code);
			cm.on("beforeChange", this.onBeforeChange);
			cm.on("change", this.onChange);
			cm.on("beforeSelectionChange", this.onBeforeSelectionChange);
			cm.on("cursorActivity", this.onDéplacerCurseur);
			cm.on("gutterClick", this.onClickGutter);
			this.cm = cm;
			if (!this.xray) {
				this.traiterZones();
			}
			this.afficherIndicateurs();
		},
		changerTentative(){
			this.cm.off("change", this.onChange);
			this.cm.doc.setValue(this.$store.getters.tentative.code);
			this.$store.dispatch("setLigneCommentairesAffichés", -1);

			if(this.tentative.date_soumission){
				// Nouvelle tentative chargée
				this.afficherIndicateurs();
			}
			else{
				// Ébauche initiale
				this.enleverIndicateurs();
			}

			this.traiterZones();

			this.cm.on("change", this.onChange);
		},
		onChange(cm, changeObj) {
			if(!this.tentative.modifiée){
				this.enleverIndicateurs();
				this.$store.dispatch("setTentativeModifiée");
			}

			if( this.sauvegardeActivée) {
				this.texteModifié();
			}

			this.$store.dispatch("mettreAjourCode", cm.doc.getValue());

			this.traiterZones();

			const marks = cm.doc.findMarksAt(changeObj.from);
			if (marks.length === 0) return;
			const mark = marks[0];
			if (mark.lines.length === 0) return;

			// Enlève la ou les premières espaces
			const ligne = mark.lines[0];
			if (ligne.text.indexOf("+TODO ") > 0 && ligne.text.indexOf("-TODO") > 0) {
				const range = mark.find();
				const matches = ligne.text.match(/(?<=\+TODO)(.+?)(?=-TODO)/);
				if (!matches) return;
				const remplacement = matches[1];
				if (remplacement.trim() != "" && remplacement.trim() !== remplacement) {
					cm.doc.replaceRange(remplacement.trim(), range.from, range.to);
				}
			}
		},

		onBeforeChange(cm, changeObj) {
			var markers = cm.doc.findMarksAt(changeObj.from);
			if (markers.length === 0) return;

			const mark = markers[0].find();

			// Si on a inséré un \n dans un todo en ligne
			if (mark.from.line == mark.to.line && changeObj.origin == "+input" && changeObj.text.join("") == "") {
				changeObj.cancel();
				return;
			}

			// Si la zone marquée a été effacée
			if (
				mark.from.line == changeObj.from.line &&
				mark.to.line == changeObj.to.line &&
				mark.from.ch == changeObj.from.ch &&
				mark.to.ch == changeObj.to.ch &&
				changeObj.text == ""
			) {
				changeObj.update(mark.from, mark.to, " ");
			}
		},

		onBeforeSelectionChange(cm, changeObj) {
			if (changeObj.ranges.length == 0) return;
			const ranges = changeObj.ranges.filter((r) => r.anchor != r.head);
			var n_ranges = [];
			ranges.forEach((range) => {
				const markers = cm.doc.findMarks(range.anchor, range.head);
				n_ranges.push(...this.rogner(range, markers));
			});

			if (n_ranges.length > 0) changeObj.update(n_ranges);
			else {
				changeObj.update([changeObj.ranges[0]]);
			}
		},
		rogner(range, markers) {
			var ranges = [];
			var début = range.anchor;
			var marques = [];
			var pile = [];

			markers.forEach((m) => {
				if (m.readonly || m.collapsed) {
					const marker = m.find();
					marques.push({ ...marker.from, type: "début" });
					marques.push({ ...marker.to, type: "fin" });
				}
			});
			marques.sort((a, b) => a.line - b.line || a.ch - b.ch || a.type > b.type);
			marques.forEach((marker) => {
				if (marker.type == "début") {
					if (pile.length == 0 && début != null) {
						ranges.push({ anchor: début, head: marker });
						début = null;
					}
					pile.push(marker);
				} else {
					pile.pop();
					if (pile.length == 0) début = marker;
				}
			});

			if (début != null) ranges.push({ anchor: début, head: range.head });

			return ranges;
		},

		beforeWindowUnload() {
			if (this.indicateurModifié || this.indicateurSauvegardeEnCours) return "";
		},

		traiterZones() {
			if (!this.zonesTraitées && !this.xray) {
				zones.cacherHorsVisible(this.cm.doc);
				zones.désactiverHorsTodo(this.cm.doc, this.$store.getters.thèmeSombre ? "#272822" : "white");
				this.zonesTraitées = true;
			}
		},

		async sauvegarder() {
			if (this.indicateurModifié && !this.indicateurSauvegardeEnCours) {
				this.indicateurSauvegardeEnCours = true;
				try {
					await this.$store.dispatch("mettreAjourSauvegarde");
					this.indicateurModifié = false;
				} catch (erreur) {
					console.log("ERREUR de sauvegarde : " + erreur);
				} finally {
					this.indicateurSauvegardeEnCours = false;
					clearTimeout(this.sauvegardeAutomatique);
					this.sauvegardeAutomatique = null;
				}
			}
		},

		texteModifié() {
			if (!this.indicateurModifié || !this.sauvegardeAutomatique) {
				this.sauvegardeAutomatique = setTimeout(this.sauvegarder, import.meta.env.VITE_DELAI_SAUVEGARDE);

				this.indicateurModifié = true;
			}
		},
		créerMarqueur(icone="mdi-comment-outline", color="var(--aqua)") {
			var marker = document.createElement("div");
			marker.style.color = color;
			marker.style.transform = "scale(-1, 1)";
			marker.style.width = "fit-content";
			marker.innerHTML = `<i class="${icone} mdi v-icon"></i>`;
			return marker;
		},
		onDéplacerCurseur(cm){
			const ligne = cm.doc.getCursor().line;
			this.$store.dispatch("setLigneNouveauCommentaire", ligne);
		},
		onClickGutter( _, ligne ){
			const n_commentaire_sur_ligne = this.commentaires.filter( (commentaire) => commentaire.numéro_ligne == ligne ).length;

			this.démarquerLigneCommentée( this.$store.getters.ligneCommentairesAffichés );
			this.cm.doc.setCursor(ligne);

			if( ligne == this.ligneCommentairesAffichés ){
				this.$store.dispatch("setLigneCommentairesAffichés", -1);
			}
			else {
				this.$store.dispatch("setLigneCommentairesAffichés", ligne);
				this.$store.dispatch("setLigneNouveauCommentaire", ligne);
			}

			if( !this.$store.getters.tentative.modifiée && n_commentaire_sur_ligne == 0 ){
				this.marquerLigneCommentée( ligne );
				this.$emit("commentaire");
			}
		},
		marquerLignesCommentées( old, ligne ){
			this.démarquerLigneCommentée( old );

			if( ligne>=0 ){
				this.marquerLigneCommentée( ligne );
			}
		},
		marquerLigneCommentée( ligne ){
			if( !(ligne >= 0)) return;

			this.cm.doc.setGutterMarker(ligne, "breakpoints", this.créerMarqueur("mdi-comment"));
		},
		démarquerLigneCommentée( ligne ){
			if( !(ligne >= 0)) return;

			const n_commentaires_sur_ligne = this.commentaires.filter( (commentaire) => commentaire.numéro_ligne == ligne ).length;

			if(n_commentaires_sur_ligne > 0) {
				this.cm.doc.setGutterMarker(ligne, "breakpoints", this.créerMarqueur("mdi-comment-outline"));
			}
			else{
				this.cm.doc.setGutterMarker(ligne, "breakpoints", null);
			}
		},
		enleverIndicateurs(){
			this.cm.doc.clearGutter("breakpoints");
		},
		afficherIndicateurs() {
			this.enleverIndicateurs();
			this.commentaires.forEach((element) => {
				this.cm.doc.setGutterMarker(element.numéro_ligne, "breakpoints", this.créerMarqueur( this.ligneCommentairesAffichés == element.numéro_ligne ? "mdi-comment" : "mdi-comment-outline"));
			});
		},
	},
};
