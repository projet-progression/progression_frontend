const mod = (n, m) => (n % m + m) % m;

export default {
	name: "TitreSéquence",
	computed: {
		titre() {
			return this.$store.getters.séquence.titre;
		},
		questions(){
			return this.$store.getters.séquence.questions;
		},
		uri(){
			return this.$store.getters.uri;
		},
	},
	methods:{
		rechercherQuestions(questions, dec) {
			const uris=Object.keys(questions);
			var prochain=uris[mod(dec, uris.length)];

			for(var index=0; index<uris.length; index++){
				if(uris[index] == this.uri){
					prochain = uris[mod((index+dec), uris.length)];
					break;
				}
			}
			this.$store.dispatch("setUri", prochain);
			this.$store.dispatch("récupérerQuestion", prochain);
		},
		questionSuivante(){
			this.rechercherQuestions( this.questions, 1 );
		},
		questionPrécédente() {
			this.rechercherQuestions( this.questions, -1 );
		},
	}
};
