export default {
	name: "MenuSéquence",
	computed: {
		uri() {
			return this.$store.getters.uri;
		},
		user() {
			return this.$store.getters.user;
		},
		séquence() {
			return this.$store.getters.séquence;
		},
		avancements() {
			return this.$store.getters.user.avancements[this.id_séquence].avancements;
		},
		pourcentage_réussite() {
			if(!this.avancements) return 0;

			const nb_items = Object.keys(this.avancements).length;
			if(nb_items == 0) return 0;

			return  Math.ceil(Object.values(this.avancements).filter( (avancement) => avancement.état == "réussi" ).length / nb_items * 100);
		},
		id_séquence() {
			return `${this.user.username}/${this.séquence.id}`;
		},
		id_question() {
			return `${this.user.username}/${this.uri}`;
		}
	},
	methods: {
		async selectionQuestion(id) {
			console.log(id);
			const index = id.split("/")[1];
			await this.$store.dispatch("setUri", index);
			await this.$store.dispatch("récupérerQuestion", index);
		},
	}
};
