export default {
	props: {
		créerCommentaire: Boolean
	},
	computed: {
		commentaires() {
			return this.$store.getters.commentaires;
		},
		n_commentaires() {
			return this.$store.getters.commentaires.length;
		},
		user() {
			return this.$store.getters.user;
		},
		ligneCommentairesAffichés() {
			return this.$store.getters.ligneCommentairesAffichés;
		},
		tentative() {
			return this.$store.getters.tentative;
		},
		ligneNouveauCommentaire() {
			return this.$store.getters.ligneNouveauCommentaire;
		},
		raccourcis() {
			return this.$store.getters.raccourcis;
		}
	},
	watch: {
		créerCommentaire: function () {
			this.$refs.submitBtn.focus();
		},
		ligneCommentairesAffichés: function () {
			this.filtrerCommentairesParLigne();
		},
		n_commentaires: function () {
			this.filtrerCommentairesParLigne();
		},
	},
	data() {
		return {
			message: "",
			commentairesParLigne: [],
		};
	},
	methods: {
		formatDate(timestamp) {
			const dateObject = new Date(parseInt(timestamp) * 1000);
			return dateObject.toLocaleString();
		},
		peutÊtreSoumis() {
			return this.message.trim() !== "" && this.ligneCommentairesAffichés > -1;
		},
		peutÊtreModifié() {
			return this.ligneCommentairesAffichés!==-1 && !this.tentative.modifiée;
		},
		async soumettreCommentaire() {
			await this.$store.dispatch("créerCommentaire", {
				url: this.$store.getters.tentative.liens.commentaires,
				message: this.message,
				créateur: this.user.username,
				numéro_ligne: this.ligneCommentairesAffichés,
				tokenRessources: this.$store.getters.tokenRessources,
			});
			this.message = "";
		},
		filtrerCommentairesParLigne() {
			this.commentairesParLigne = this.commentaires.filter( (item) => item.numéro_ligne == this.ligneCommentairesAffichés ).sort((a, b) => a.date - b.date);
		},
		focusCommentaire(){
			this.$store.dispatch("setLigneCommentairesAffichés", this.$store.getters.ligneNouveauCommentaire);
		},
	},
};
