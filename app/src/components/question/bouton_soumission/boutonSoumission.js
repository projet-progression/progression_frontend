export default {
	name: "BoutonSoumission",
	computed: {
		question_type() {
			return this.$store.getters.question_type;
		},
		raccourcis() {
			return this.$store.state.raccourcis;
		},
		envoiEnCours() {
			return this.$store.state.envoiTentativeEnCours;
		},
		erreurCallback() {
			return this.$store.state.erreur_callback;
		},
		soumission_permise() {
			const tkres = this.$store.state.tokenRessources;
			return !tkres || tkres.length == 0;
		}

	},
	methods: {
		validerTentative() {
			if(this.soumission_permise) {
				this.$store.dispatch("soumettreTentative", false );
			}
		},
	}
};
