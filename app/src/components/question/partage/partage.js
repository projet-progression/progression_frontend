export default {
	name: "Partage",
	data() {
		return {
			copié: false,
			pressePapier: navigator.clipboard,
		};
	},
	computed: {
		état_réussi() {
			return this.$store.getters.avancement.état == "réussi";
		},
		tentative() {
			return this.$store.getters.tentative;
		},
		publique()
		{
			return this.tentative.publique;
		},
		uri() {
			return this.$store.getters.uri;
		},
		lang() {
			return this.$store.getters.tentative.langage;
		},
		partageable() {
			return this.tentative.date_soumission && this.état_réussi;
		},
		indicateur_partage() {
			return this.$store.getters.indicateursDeFonctionnalité("partage");
		},
	},
	watch: {
		"tentative.tests_réussis"(estRéussi) {
			if (estRéussi === this.$store.state.question.tests.length) {
				this.exerciceReussi = true;
			}
		},
	},
	methods: {
		async copierLienTentative() {
			const lienPartage = window.location.href + "&tkres=" + (await this.$store.dispatch("récupérerTokenPartage")).jwt;

			if(this.pressePapier) {
				this.pressePapier.writeText( lienPartage );

				this.copié=true;
				setTimeout( () =>{
					this.copié=false;
				}, 1000 );
			}
			else
			{
				console.log(lienPartage);
				alert("Copié à la console");
			}
		},
		async basculerSolutionPublique() {
			this.tentative.publique = !this.tentative.publique;
			this.$store.dispatch("mettreÀJourTentative", { publique: this.tentative.publique });
		},
		allerAuxSolutions(){
			this.$router.push({
				name: "SolutionsPartagées",
				query: {
					uri: this.uri,
					lang: this.lang
				}
			});
		}
	}
};
