import parseMD from "@/util/parse";
import {ÉTAT} from "@/util/constantes";

export default {
	name: "Enonce",
	props: {
		énoncéPleinÉcran: Boolean,
		énoncéSemiÉcran: Boolean
	},
	data() {
		return {tab: null};
	},
	emits: ["ajustéPanneauÉnoncé"],
	computed: {
		état_réussi() {
			return this.$store.state.avancement.état == ÉTAT.RÉUSSI;
		},
		question() {
			return new Proxy(this.$store.state.question ?? this.$store.state.séquence, {
				get: function (obj, prop) {
					if(prop != "énoncé"){
						return obj[prop];
					}
					const énoncé = obj["énoncé"];
					if(!Array.isArray(énoncé)){
						return [{
							titre: null,
							texte: parseMD(énoncé)
						}];
					}
					return énoncé.map( page => ({
						titre: page.titre,
						texte: parseMD(page.texte)
					}));
				},
			});
		},
	},
};
