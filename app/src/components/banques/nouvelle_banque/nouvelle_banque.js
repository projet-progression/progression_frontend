import {validerUrl} from "@/util/url.js";

export default {
	name: "NouvelleBanque",
	emits: {
		ajouterBanque: Object
	},
	props: {
		montrer: Boolean,
		nouvelle_banque: Object
	},
	data() {
		return {
			visible: false,
			validations: {
				url: val => validerUrl(val) || this.$t("banque_question.format_url"),
				obligatoire: val => val != "" || this.$t("banque_question.champ_manquant")
			},
			banqueValide: false
		};
	},

	watch: {
		montrer(){
			this.visible = true;
		}
	},

	methods: {
		ajouterBanque() {
			this.$emit("ajouterBanque", this.nouvelle_banque );
			this.visible=false;
		}
	}
};
