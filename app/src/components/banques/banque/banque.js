import Questions from "@/components/banques/questions/questions.vue";

export default {
	components: {
		Questions
	},
	name: "Banque",
	props: {
		banque: Object,
	},
	data() {
		return {
			étendue: [],
		};
	},
	computed: {
		sous_banques() {
			return this.banque?.banques;
		},
		nom() {
			return this.banque?.nom ?? this.banque?.nom;
		},
		petit_écran(){
			return window.innerWidth < 800;
		}
	},

	methods: {
		async récupérerQuestions() {
			if(this.banque.banques == null && this.banque?.liens?.self){
				const n_banque = await this.$store.dispatch("récupérerBanqueEtQuestions",{ url: this.banque.liens.self });
				this.banque.banques = n_banque.banques;
				this.banque.questions = n_banque.questions;
			}
		},

		sélectionner(banque){
			this.$store.dispatch("sélectionnerBanque", banque);
		}
	},

	watch : {
		étendue() {
			if (this.étendue){
				this.récupérerQuestions();
			}
		}
	}
	
};
