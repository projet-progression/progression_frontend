import {obtenirUri, atob_url} from "@/util/url.js";

export default {
	name: "Questions",
	props: {
		banque: Object
	},
	computed: {
		ltik() {
			return this.$store.getters.ltik;
		}
	},
	methods: {
		ouvrirNouvelExercice(url) {
			this.$router.push({
				name: "Question",
				query: { uri: obtenirUri( url ) }
			});
		},
		url_décodé(id) {
			return atob_url( id );
		}
	}
};
