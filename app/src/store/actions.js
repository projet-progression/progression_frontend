import {
	authentifierApi,
	inscrireApi,
	callbackGrade,
	getConfigServeurApi,
	getAvancementApi,
	getBanquesApi,
	getBanqueEtQuestionsApi,
	postBanqueApi,
	getTousAvancementsApi,
	getQuestionApi,
	getTentativeApi,
	getTokenIDApi,
	getTokenRessourceApi,
	getUserApi,
	getUserAvecTentativesApi,
	patchAvancementApi,
	putAvancementApi,
	patchUserApi,
	postCommentaireApi,
	postRésultat,
	postSauvegardeApi,
	postTentative,
	postTentativeSys,
	postAuthKey,
	patchTentativeApi
} from "@/services/index.js";

import {TYPE, ÉTAT} from "@/util/constantes.js";
import {récupérerOuCréerAvancement} from "./avancements.js";
import {i18n, sélectionnerLocale} from "@/util/i18n";
import {copie_profonde, AuthentificationError} from "@/util/commun.js";
import {lock} from "@/util/lock";
import jwt_decode from "jwt-decode";
import store from "./store.js";

var validateur = (v) => v;

const valider = async function (promesse) {
	return validateur(promesse());
};

const API_URL = import.meta.env.VITE_API_URL;

async function rafraîchirToken() {
	const authKey = récupérerCléSauvegardée();
	const username = récupérerUsername();

	if (username && authKey) {
		try {
			const lien_tokens = store.getters.user?.liens?.tokens ?? store.getters.configServeur?.liens?.tokens;
			return lien_tokens ? await getTokenIDApi(lien_tokens, username, authKey) : null;
		}
		catch (err) {
			console.log(err);
			throw new AuthentificationError("Erreur d'authentification.");
		}
	} else {
		const err = new AuthentificationError("Clé d'authentification non disponible.");
		console.log(err);
		throw err;
	}
}

function récupérerUsername() {
	return sessionStorage.getItem("username") ?? localStorage.getItem("username");
}

function récupérerCléSauvegardée() {
	return sessionStorage.getItem("authKey_nom") ?? localStorage.getItem("authKey_nom");
}

function générerAuthKey(user, token, expiration = 0) {
	const clé_id = "LTIauthKey_" + randomID();

	return {
		nom: clé_id,
		portée: 1,
		expiration: expiration,
	};
}

function randomID() {
	// Math.random should be unique because of its seeding algorithm.
	// Convert it to base 36 (numbers + letters), and grab the first 9 characters
	// after the decimal.
	return Math.random().toString(36).substr(2, 9);
}

function récupérerListeAvecPourcentageRéussi(élémentsRéussis) {

	const réussis = Object.keys(élémentsRéussis);
	const listeRéussis = [];
	for (let langage of réussis) {
		let pourcentage = récupérerPourcentageRéussi(élémentsRéussis, langage);
		listeRéussis.push([langage, pourcentage]);
	}

	return listeRéussis;

}

function récupérerPourcentageRéussi(réussis, langage) {
	var totalRéussi = 0;
	var pourcentage = 0;
	for (var tentative in réussis) {
		totalRéussi += réussis[tentative];
	}
	pourcentage = (100 / totalRéussi) * réussis[langage];
	return pourcentage.toFixed(1);
}


function sélectionnerTentative(avancement, question, options) {
	if (question?.sous_type == TYPE.PROG)
		return sélectionnerTentativeProg(avancement, question, options.lang_défaut);

	if (question?.sous_type == TYPE.SYS) {
		return sélectionnerTentativeSys(avancement, question);
	}
}

function sélectionnerTentativeProg(avancement, question, lang_défaut) {
	var tentative;

	var urlParams = new URLSearchParams(window.location.search);
	if(urlParams.has("tentative")){
		const id = urlParams.get("tentative");
		const trouvée = avancement.tentatives.find( (t) => t.date_soumission == id );
		if(trouvée) return trouvée;
	}

	if (Object.keys(avancement.sauvegardes).length > 0) {
		var datePlusRecente = 0;
		for (var key in avancement.sauvegardes) {
			if (avancement.sauvegardes[key].date_sauvegarde > datePlusRecente) {
				tentative = {
					code: avancement.sauvegardes[key].code,
					langage: key,
					resultats: [],
					modifiée: true
				};
				datePlusRecente = avancement.sauvegardes[key].date_sauvegarde;
			}
		}
	} else if (avancement.tentatives.length > 0) {
		tentative = avancement.tentatives[0];
	} else if (question) {
		var ebauches = question.ebauches;
		if (ebauches[lang_défaut]) {
			tentative = ebauches[lang_défaut];
		} else {
			tentative = ebauches[Object.keys(ebauches)[0]];
		}
	}

	return tentative;
}

function sélectionnerTentativeSys(avancement) {
	if (avancement.tentatives.length > 0) {
		return avancement.tentatives[0];
	}
	else return { conteneur_id: "", url_terminal: "" };
}

async function setValidateur(v) {
	validateur = v;
}

async function setUnleash({ commit }, unleash) {
	commit("setUnleash", unleash);
}

async function setErreurs({ commit, state }, erreurs) {
	if(state.erreurs.length>=5) state.erreurs.splice(0,1);
	state.erreurs.push(erreurs);
	commit("setErreurs", state.erreurs);
}

async function réinitialiserErreurs({ commit }) {
	commit("setErreurs", []);
}

async function setErreurCallback({ commit }, erreur) {
	commit("setErreurCallback", erreur);
}

async function getToken({ commit, getters }) {
	try{
		return await lock( "token", async () => {
			var token = getters.obtenirToken();
			if(!token){
				token = await rafraîchirToken();
				commit("setToken", token);
			}
			return token;
		});
	}
	catch (e) {
		commit("setToken", null);
		throw e;
	}
}

async function récupérerConfigServeur({ commit, getters }, urlConfig) {
	return valider(async () => {
		const token = getters.obtenirToken();

		var config=null;
		if(token){
			config = await getConfigServeurApi(urlConfig, token);
		}
		else{
			const authKey = récupérerCléSauvegardée();
			const username = récupérerUsername();
			if( username && authKey ) {
				try{
					config = await getConfigServeurApi(urlConfig, null, username, authKey );
				}
				catch(err){
					config = await getConfigServeurApi(urlConfig);
				}
			}
			else{
				config = await getConfigServeurApi(urlConfig);
			}
		}

		commit("setConfigServeur", config);
		return config;
	});
}

async function inscrire( {commit, getters} ,  params ){
	const urlInscription = getters.configServeur.liens.inscrire;
	const courriel = params.courriel;
	const username = params.identifiant;
	const motDePasse = params.password;

	commit("setEnChargement", true);
	try {
		const user = await inscrireApi(urlInscription, username, courriel, motDePasse);
		commit("setUser", user);
		commit("setUsername", user.username);
		return user;
	}
	finally {
		commit("setEnChargement", false);
		commit("updateAuthentificationEnCours", false);
	}

}

async function authentifier({ commit }, params) {
	const urlAuth = API_URL;
	const identifiant = params.identifiant;
	const password = params.password;
	const persister = params.persister;
	const domaine = params.domaine;
	commit("updateAuthentificationEnCours", true);

	commit("setEnChargement", true);
	try {
		const token = await authentifierApi(urlAuth, identifiant, password, domaine);
		commit("setToken", token);

		// Obtenir l'utilisateur
		const user = await this.dispatch("récupérerUser", token.liens.user);

		const storage = persister ? localStorage : sessionStorage;
		storage.setItem("username", user.username);

		// Obtenir la clé d'authentification
		var clé = générerAuthKey(user, token, persister ? 0 : (Math.floor(Date.now() / 1000 + parseInt(import.meta.env.VITE_API_AUTH_KEY_TTL))));

		const authKey = await postAuthKey({ url: user.liens.clés, clé: clé }, identifiant, password, domaine);

		storage.setItem("authKey_nom", authKey.nom);

		return token;
	}
	finally {
		commit("setEnChargement", false);
		commit("updateAuthentificationEnCours", false);
	}
}

async function déconnexion({commit}){
	sessionStorage.removeItem("authKey_nom");
	localStorage.removeItem("authKey_nom");
	localStorage.removeItem("username");
	sessionStorage.removeItem("username");

	commit("setUsername", null);
	commit("setUser", null);
	commit("setToken", null);
	commit("setConfigServeur", null);
}

async function récupérerTokenPartage({ state }){

	const username = récupérerUsername();
	const uri = state.uri;
	const timestamp = state.tentative.date_soumission;

	const modèle_token = {
		expiration: "0",
		fingerprint: false,
		data : {
			url_avancement: API_URL + `avancement/${username}/${uri}`,
		},
		ressources : {
			avancement: {
				url: `^avancement/${username}/${uri}$`,
				method: "^GET$"
			},
			tentative: {
				url: `^tentative/${username}/${uri}/${timestamp}$`,
				method: "^GET$"
			},
			commentaire: {
				url: `^commentaire/${username}/${uri}/${timestamp}/.*$`,
				method: "^GET$"
			},
			commentaires: {
				url: `^tentative/${username}/${uri}/${timestamp}/commentaires$`,
				method: "^POST$"
			}
		}
	};

	return valider(async () => {
		const token = await this.dispatch("getToken");
		const lien_tokens = store.getters.user.liens.tokens;
		return await getTokenRessourceApi(lien_tokens, token, modèle_token);
	});
}

async function récupérerTokenScore({ commit, state }) {
	const username = state.username;
	const uri = state.uri;
	const id = `${username}/${uri}`;
	const timestamp = state.tentative.date_soumission;

	const modèle_token = {
		expiration: "0",
		fingerprint: false,
		data : {
			url_avancement: API_URL + `avancement/${id}`,
			url: window.location.origin + `/question?uri=${uri}&tentative=${timestamp}`,
		},
		ressources : {
			avancement: {
				url: `^avancement/${id}$`,
				method: "^GET$"
			},
			tentative: {
				url: `^tentative/${id}/.*$`,
				method: "^GET$"
			},
			commentaire: {
				url: `^commentaire/${id}/.*$`,
				method: "^GET$"
			},
			commentaires: {
				url: `^tentative/${id}/.*/commentaires$`,
				method: "^POST$"
			}
		}
	};

	return valider(async () => {
		const lien_tokens = store.getters.user.liens.tokens;
		const token = await this.dispatch("getToken");
		const token_score = await getTokenRessourceApi(lien_tokens, token, modèle_token);

		commit("setTokenScore", token_score);
		return token_score;
	});
}

async function mettreÀJourUser( {commit}, params ){
	return valider(async () => {
		const token = params.token?.jwt ? params.token : await this.dispatch("getToken");
		const user =  await patchUserApi( { url: params.url, user: params.user }, token );
		commit("setUser", user);
		return user;
	});
}

async function récupérerUser({ commit }, urlUser) {
	return valider(async () => {
		commit("setEnChargement", true);
		try {
			const token = await this.dispatch("getToken");
			const user = await getUserApi(urlUser, token);

			commit("setUsername", user.username);
			commit("setUser", user);
			if(!user.préférences.locale){
				user.préférences.locale = sélectionnerLocale(null);
			}
			commit("setPréférences", user.préférences);

			return user;
		}
		finally {
			commit("setEnChargement", false);
		}
	});
}

async function obtenirUser( _, urlUser) {
	const token = await this.dispatch("getToken");
	return await getUserApi(urlUser, token);
}

async function validerCourriel( {commit}, params ){
	return valider(async () => {
		const tokenRessources = params.tokenRessources;
		const user =  await patchUserApi( { url: params.url, user: params.user }, null, tokenRessources );
		commit("setUser", user);
		return user;
	});
}

async function récupérerQuestion({ commit, getters }, uri) {
	if(!getters.séquence?.questions || !(getters.uri in getters.séquence.questions)){
		commit("setSéquence", null);
		commit("setQuestion", null);
		setAvancement( {commit, getters}, null);
		commit("setTentative", null);
	}
	return valider(async () => {
		commit("setEnChargement", true);
		const token = await this.dispatch("getToken");
		try {
			const question = await getQuestionApi(API_URL + "/question/" + uri, token);
			if(question.sous_type == TYPE.SEQ) {
				commit("setSéquence", question);
			}
			else{
				if(!getters.séquence){
					commit("setSéquence", {
						id: question.id,
						titre: "Question",
						questions: { [`${getters.user.username}/${getters.uri}`]: question },
					});
				}
				commit("setQuestion", question);
			}
			récupérerOuCréerAvancement();
			return question;
		}
		catch (e) {
			if (e?.response?.status == 422) {
				throw i18n.global.t("erreur.question_invalide");
			}
			else if (e?.response?.status == 502) {
				throw i18n.global.t("erreur.question_introuvable");
			}
			else {
				throw e;
			}
		}
		finally {
			commit("setEnChargement", false);
		}
	}
	              );
}

async function récupérerBanques({ commit }, params) {
	return valider(async () => {
		commit("setEnChargement", true);
		try {
			const token = await this.dispatch("getToken");
			const banques = await getBanquesApi(params.url, token);

			commit("setBanques", banques);
			return banques;
		}
		finally {
			commit("setEnChargement", false);
		}
	});
}

async function récupérerBanqueEtQuestions({ commit }, params) {
	return valider(async () => {
		commit("setEnChargement", true);
		try {
			const token = await this.dispatch("getToken");
			const banque = await getBanqueEtQuestionsApi(params.url, token);

			return banque.banque;
		}
		catch(e) {
			if (e?.response?.status == 422) {
				throw i18n.global.t("erreur.banque_invalide");
			}
		}
		finally {
			commit("setEnChargement", false);
		}
	});
}

async function ajouterUneBanque({ commit }, params) {
	return valider(async () => {
		commit("setEnChargement", true);
		try {
			const token = await this.dispatch("getToken");
			return await postBanqueApi(params, token);
		}
		finally {
			commit("setEnChargement", false);
		}
	});
}

async function récupérerTousAvancements({ commit }, params) {
	return valider(
		async () => {
			commit("setEnChargement", true);
			try {
				const token = await this.dispatch("getToken");
				const tokenRessources = params.tokenRessources;
				const avancements = await getTousAvancementsApi(params.url, token, tokenRessources);

				return avancements;
			}
			finally {
				commit("setEnChargement", false);
			}
		}
	);
}

async function récupérerAvancement({ commit, getters }, params) {
	return valider(
		async () => {
			commit("setEnChargement", true);
			try {
				const token = params.token?.jwt ? params.token : await this.dispatch("getToken");
				const tokenRessources = params.tokenRessources;
				const avancement = await getAvancementApi(params.url, token, tokenRessources);

				setAvancement({commit, getters}, avancement);

				return avancement;
			}
			finally {
				commit("setEnChargement", false);
			}
		}
	);
}

async function sauvegarderAvancement(_, params) {
	return valider(async () => {
		const token = await this.dispatch("getToken");
		return await patchAvancementApi(params, token);
	});
}

async function créerAvancement({ commit, getters }, params) {
	return valider(async () => {
		const token = await this.dispatch("getToken");
		const avancement = await putAvancementApi(params, token);
		setAvancement({commit, getters}, avancement);
		return avancement;
	});
}

async function créerCommentaire({commit}, params ) {
	return valider(async () => {
		const token = await this.dispatch("getToken");

		const commentaire =  await postCommentaireApi(params, token, params.tokenRessources);

		commit("ajouterCommentaire", commentaire);
	});
}

async function récupérerTentative({ commit }, params) {
	return valider(async () => {

		commit("setEnChargement", true);

		try {
			const token = params.token?.jwt ? params.token : await this.dispatch("getToken");
			const tentative = await getTentativeApi(params.urlTentative, token, params.tokenRessources);
			commit("setTentative", tentative);
			commit("setTentativeModifiée", false);
			return tentative;
		}
		finally {
			commit("setEnChargement", false);
		}
	});
}

async function récupérerNbRéussitesParLangage({ commit }, params) {
	var langagesRéussis = new Object();
	var ceLangageEstRéussi = new Object();

	var token;
	var user;
	return valider(async () => {
		token = params.token?.jwt ? params.token : await this.dispatch("getToken");
		user = await getUserAvecTentativesApi(params.url, token);

		for (var id in user.avancements) {

			const avancement = user.avancements[id];
			const tentatives = avancement.tentatives;
			for (let tentative of tentatives) {
				ceLangageEstRéussi[tentative.langage] = false;
			}
			for (let tentative of tentatives) {
				if (tentative.réussi) {
					if (tentative.langage in langagesRéussis) {
						if (ceLangageEstRéussi[tentative.langage] == false) {
							langagesRéussis[tentative.langage] += 1;
							ceLangageEstRéussi[tentative.langage] = true;
						}
					}
					else {
						if (ceLangageEstRéussi[tentative.langage] == false) {
							langagesRéussis[tentative.langage] = 1;
							ceLangageEstRéussi[tentative.langage] = true;
						}
					}
				}
			}
		}
		langagesRéussis = récupérerListeAvecPourcentageRéussi(langagesRéussis);
		commit("setNbRéussitesParLangage", langagesRéussis);
		return langagesRéussis;
	}
	              );
}

async function récupérerDifficultésRéussies({ commit }, params) {
	var difficultésRéussies = new Object();

	var token;
	var user;

	return valider(async () => {
		token = params.token?.jwt ? params.token : await this.dispatch("getToken");
		user = await getUserApi(params.url, token);

		for (const idAvancement in user.avancements) {
			const avancement = user.avancements[idAvancement];
			if (avancement.niveau === null || avancement.niveau === "") {
				avancement.niveau = "[N/D]";
			}
			if (avancement.état == ÉTAT.RÉUSSI) {
				if (avancement.niveau in difficultésRéussies) {
					difficultésRéussies[avancement.niveau] += 1;
				}
				else {
					difficultésRéussies[avancement.niveau] = 1;
				}
			}
		}
		difficultésRéussies = récupérerListeAvecPourcentageRéussi(difficultésRéussies);
		commit("setDifficultésRéussies", difficultésRéussies);
		return difficultésRéussies;
	}
	              );
}

async function soumettreTentative({ commit, getters }, première = false) {
	commit("updateEnvoieTentativeEnCours", true);
	commit("setRésultats", [] );
	commit("setFeedback", null );
	commit("setErreurCallback", null);

	return valider(async () => {
		try {
			const token = await this.dispatch("getToken");
			const avancement = getters.avancement;
			var tentative = null;
			if(getters.question_type == TYPE.SYS){
				tentative = await postTentativeSys({tentative: getters.tentative, urlTentative: getters.avancement.liens.tentatives}, token);
				if(première) {
					tentative.resultats = null;
				}
			}
			else{
				tentative = await postTentative({tentative: getters.tentative, urlTentative: getters.avancement.liens.tentatives}, token);
			}

			commit("setTentative", tentative);
			commit("updateEnvoieTentativeEnCours", false);
			avancement.tentatives.unshift(tentative);
			if (avancement.état != ÉTAT.RÉUSSI) {
				avancement.état = tentative.réussi ? ÉTAT.RÉUSSI : ÉTAT.NON_RÉUSSI;
			}

			if (getters.cb_succes) {
				try{
					const tokenScore = await store.dispatch("récupérerTokenScore");
					await callbackGrade(getters.cb_succes, getters.uri, tokenScore );
				}
				catch(e){
					if(e.response.status == 401) {
						commit("setErreurCallback", i18n.global.t("retroaction_tentative.erreurCallback401"));
					}
					else if(e.response.status >= 400) {
						commit("setErreurCallback", i18n.global.t("retroaction_tentative.erreurCallbackAutre"));
					}
				}
			}
			return tentative;
		}
		catch (e) {
			commit("updateEnvoieTentativeEnCours", false);

			if (e?.response?.status == 400) {
				throw i18n.global.t("erreur.tentative_intraitable");
			}
			else {
				throw (e);
			}
		}
	}
	              );
}

function soumettreTestUnique({commit, getters}, params) {
	const indexTestSélectionné = params.index;
	
	return valider( async () => {
		try {
			const token = await this.dispatch("getToken");
			const résultat = await postRésultat({tentative: getters.tentative, test: params.test, index: params.index, url: getters.question.liens.résultats}, token);

			if( !getters.envoiTentativeEnCours ) {
				commit("setRésultat", {index: indexTestSélectionné, résultat: résultat});
			}
		}
		catch (e) {
			if(e?.response?.status==400) {
				throw i18n.global.t("erreur.tentative_intraitable");
			}
			else{
				throw(e);
			}
		}
	}
	              );
}

function réinitialiserConteneur({commit, getters}) {
	return valider( async () => {
		try {
			commit("setConteneurEnChargement", true);
			const token = await this.dispatch("getToken");
			const tentative = await postTentativeSys({tentative: {conteneur_id: ""}, urlTentative: getters.avancement.liens.tentatives}, token);

			commit("setTentative", tentative);
		}
		catch (e) {
			if(e?.response?.status==400) {
				throw i18n.global.t("erreur.tentative_intraitable");
			}
			else{
				throw(e);
			}
		}
		finally {
			commit("setConteneurEnChargement", false);
		}
	}
	              );
}

async function mettreAjourSauvegarde({ commit, getters }) {
	const params = {
		url: getters.avancement.liens.sauvegardes,
		code: getters.tentative.code,
		langage: getters.tentative.langage,
	};

	return valider(async () => {

		const token = await this.dispatch("getToken");
		const sauvegarde = await postSauvegardeApi(params, token);

		if (sauvegarde) {
			commit("setSauvegarde", sauvegarde);
			return sauvegarde;
		}
	}
	              );
}

function mettreAjourCode({ commit }, code) {
	commit("updateCodeTentative", code);
}

function réinitialiser({ commit, getters }, langage_p) {
	const langage = langage_p ?? getters.tentative.langage;
	commit("setTentative", {
		langage: langage,
		code: this.getters.question.ebauches[langage].code,
		resultats: [],
		tests_réussis: null,
		modifiée: true
	});
}

function réinitialiserTests({ commit, state }){
	commit("setTests", copie_profonde( state.testsInitiaux ) );
}

function setToken({ commit }, token) {
	if (token) {
		try {
			const token_décodé = jwt_decode(token);
			if (token_décodé.username) {
				commit("setToken", token);
				commit("setUsername", token_décodé.username);
			}
		} catch (e) {
			commit("setToken", null);
			commit("setUsername", null);
			commit("setUser", null);
		}
	}
	else {
		commit("setToken", null);
	}
}

async function setUri({commit, state}, uri){
	if (uri != state.uri) {
		commit("setUri", uri);
		this.dispatch("récupérerQuestion", uri);
	}
}

function setUser({ commit }, user) {
	commit("setUser", user);
}

function setLangageDéfaut({ commit }, langageDéfaut) {
	commit("setLangageDéfaut", langageDéfaut);
}

function setDémo({ commit }, val) {
	commit("setDémo", val);
}

function setCallbackSucces({ commit }, cb_succes) {
	commit("setCallbackSucces", cb_succes);
}

function setLtik({ commit }, ltik) {
	commit("setLtik", ltik);
}

function setTokenRessources({ commit }, tokenRessources) {
	commit("setTokenRessources", tokenRessources);
}

function setUsername({ commit }, username) {
	commit("setUsername", username);
}

function setAuthentificationErreurHandler({ commit }, authentificationErreurHandler) {
	commit("setAuthentificationErreurHandler", authentificationErreurHandler);
}

function setThèmeSombre({ commit }, val) {
	commit("setThèmeSombre", val);
}

function basculerThèmeSombre({ getters }) {
	this.dispatch("setPréférences", {
		apparence_thème: getters.thèmeSombre ? "clair" : "sombre",
		éditeur_thème: getters.thèmeSombre ? "default" : "monokai"
	});
}

function basculerLocale({ commit, getters }){
	const locale = getters.locale =="fr" ? sélectionnerLocale("en") : sélectionnerLocale("fr");
	commit("setLocale", locale);
	this.dispatch("setPréférences", {
		locale: locale,
	});
}

function setAvancement({ commit, getters }, val) {
	commit("setAvancement", val);
	if(val){
		const tentative = sélectionnerTentative(val, getters.question, { lang_défaut: getters.langageDéfaut });

		commit("setTentative", tentative);
	}
}

function setModeAffichage({ commit }, val) {
	commit("setModeAffichage", val);
}

function basculerModeAffichage({ commit }) {
	commit("setModeAffichage", !this.state.mode_affichage );
}

function setPréférences( {commit, state, getters}, params ) {
	const préférences = {...getters.user.préférences, ...params };
	commit("setPréférences", préférences);

	return valider( async () => {
		const token = await this.dispatch("getToken");
		await patchUserApi({url: state.user.liens.self, user: {préférences: JSON.stringify(getters.user.préférences)}}, token);
	} );
}

function mettreÀJourTentative( {getters}, tentative ) {
	return valider( async () => {
		const token = await this.dispatch("getToken");
		await patchTentativeApi({url: getters.tentative.liens.self, tentative: tentative}, token);
	} );
}

function setDisposition( _ , val ) {
	this.dispatch("setPréférences", {
		disposition: val,
	});
}

function setIndicateursDeFonctionnalité({ commit }, val) {
	const toggles = [];
	for (const toggle of val) {
		toggles[toggle.name] = { enabled: toggle.enabled, variant: toggle.variant };
	}
	commit("setIndicateursDeFonctionnalité", toggles);
}

function setEnvoiTestEnCours({ commit }, val) {
	commit("setEnvoiTestEnCours", val);
}

function setEntréeTest({ commit }, val) {
	commit("setEntréeTest", val);
}

function setParamsTest({ commit }, val) {
	commit("setParamsTest", val);
}

function setTest({ commit }, val) {
	commit("setTest", val);
}

function setRésultat({ commit }, val) {
	commit("setRésultat", val);
}

function setRésultats({ commit }, val) {
	commit("setRésultats", val);
}

function setLigneCommentairesAffichés({ commit }, val) {
	commit("setLigneCommentairesAffichés", val);
}

function setLigneNouveauCommentaire({ commit }, val) {
	commit("setLigneNouveauCommentaire", val);
}

function setTentativeModifiée({ commit },) {
	commit("setTentativeModifiée", true);
}

function setTests({ commit }, val) {
	commit("setTests", val);
}

function sélectionnerBanque( {commit}, banque ){
	commit("setBanqueSélectionnée", banque);
}

export default {
	ajouterUneBanque,
	authentifier,
	basculerLocale,
	basculerModeAffichage,
	basculerThèmeSombre,
	créerAvancement,
	créerCommentaire,
	déconnexion,
	getToken,
	inscrire,
	mettreAjourCode,
	mettreAjourSauvegarde,
	mettreÀJourTentative,
	mettreÀJourUser,
	obtenirUser,
	récupérerAvancement,
	récupérerBanqueEtQuestions,
	récupérerBanques,
	récupérerConfigServeur,
	récupérerDifficultésRéussies,
	récupérerNbRéussitesParLangage,
	récupérerQuestion,
	récupérerTentative,
	récupérerTokenPartage,
	récupérerTokenScore,
	récupérerTousAvancements,
	récupérerUser,
	réinitialiser,
	réinitialiserConteneur,
	réinitialiserErreurs,
	réinitialiserTests,
	sauvegarderAvancement,
	setAuthentificationErreurHandler,
	setAvancement,
	setCallbackSucces,
	setDisposition,
	setDémo,
	setEntréeTest,
	setEnvoiTestEnCours,
	setErreurCallback,
	setErreurs,
	setIndicateursDeFonctionnalité,
	setLangageDéfaut,
	setLigneCommentairesAffichés,
	setLigneNouveauCommentaire,
	setLtik,
	setModeAffichage,
	setParamsTest,
	setPréférences,
	setRésultat,
	setRésultats,
	setTentativeModifiée,
	setTest,
	setTests,
	setThèmeSombre,
	setToken,
	setTokenRessources,
	setUnleash,
	setUri,
	setUser,
	setUsername,
	setValidateur,
	sélectionnerBanque,
	soumettreTentative,
	soumettreTestUnique,
	validerCourriel
};
