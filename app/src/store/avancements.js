import store from "@/store/index.js";
import jwt_decode from "jwt-decode";
import {ÉTAT} from "@/util/constantes";

function récupérerOuCréerAvancement() {
	if (store.state.tokenRessources) {
		récupérerAvancementPartagé(store.state.tokenRessources);
	}
	else {
		const username = store.getters.username;
		const id_avancement = username + "/" + store.getters.uri;

		récupérerOuCréerAvancementPersonnel(id_avancement);
	}
}

function récupérerAvancementPartagé( tokenRessources ){
	const tokenRessourcesDécodé = jwt_decode(tokenRessources);
	const url_avancement = tokenRessourcesDécodé.data["url_avancement"];

	store.dispatch("récupérerAvancement", {
		url: url_avancement,
		tokenRessources: tokenRessources,
	});
}

function récupérerOuCréerAvancementPersonnel(id_avancement){
	if (id_avancement in store.getters.user.avancements) {
		récupérerAvancementEtTraiterCallbacks(store.getters.user.avancements[id_avancement].liens.self);
	}
	else {
		créerAvancement(id_avancement);
	}
}

function récupérerAvancementEtTraiterCallbacks(url_avancement) {
	store.dispatch("récupérerAvancement", {
		url: url_avancement,
	}).then((avancement) => {
		if (!store.state.cb_succes) {
			récupérer_infos_callback(avancement);
		}
		else {
			sauvegarder_infos_callback(avancement);
		}
	});
}

function récupérer_infos_callback(avancement) {
	if (avancement?.extra?.cb_succes) {
		store.dispatch("setCallbackSucces", avancement.extra.cb_succes);
	}
}

function sauvegarder_infos_callback(avancement) {
	if (store.state.cb_succes != avancement?.extra?.cb_succes) {
		sauvegarderAvancement(store.state.cb_succes ? {
			extra: JSON.stringify({cb_succes: store.state.cb_succes}),
		} : {});
	}
}

function sauvegarderAvancement(avancement) {
	return store
		.dispatch("sauvegarderAvancement", {
			url: store.getters.avancement.liens.self,
			avancement
		});
}

function créerAvancement(id_avancement) {
	const avancement = {
		etat: ÉTAT.DÉBUT, ...(store.state.cb_succes ? {
			extra: JSON.stringify({cb_succes: store.state.cb_succes})
		} : {})
	};

	store.dispatch("créerAvancement", {
		url: store.getters.question?.liens.avancement ?? store.getters.séquence?.liens.avancement,
		id_avancement: id_avancement,
		avancement
	}).then((avancement_créé) => {
		store.getters.user.avancements[id_avancement] = avancement_créé;
	});
}

export {
	récupérerOuCréerAvancement,
};
