import {TYPE} from "@/util/constantes.js";

export default {
	cb_succes: state => state.cb_succes,
	avancement: state => state.avancement,
	banqueSélectionnée: state => state.banque_sélectionnée,
	user: state => state.user,
	banques: state => state.banques,
	ltik: state => state.ltik,
	username: state => state.username,
	configServeur: state => state.configServeur,
	version: state => state.configServeur.version,
	locale: state => state.user?.préférences?.locale,
	ligneCommentairesAffichés: state => state.ligneCommentairesAffichés,
	ligneNouveauCommentaire: state => state.ligneNouveauCommentaire,
	commentaires: state => state.tentative?.commentaires ?? [],
	thèmeSombre: state => state.user?.préférences?.apparence_thème == "sombre",
	erreurs: state => state.erreurs,
	raccourcis: state => state.raccourcis,
	solutionsPartagées: state => state.solutionsPartagées,
	tentative: state => state.tentative,
	envoiTentativeEnCours: state => state.envoiTentativeEnCours,
	langageDéfaut: state => state.langageDéfaut,
	//On obtient le token via une fonction pour éviter qu'il soit mis en cache
	token: () => { throw Error("Utilisez getters.obtenirToken() pour obtenir un token"); },
	obtenirToken: state => () => {
		if (state.token == null) return null;

		const token = state.token;
		const temps_courant = Math.round(Date.now() / 1000);
		// Retourne le token jusqu'à 10s avant son expiration
		return token?.timestamp && temps_courant < token.timestamp - 10 ? token.token : null;
	},
	tokenRessources: state => state.tokenRessources,
	indicateursDeFonctionnalité: state => (indicateur) => {
		return state.indicateursDeFonctionnalité[indicateur] ? true : false;
	},
	démos: state => state.user?.préférences["démos"]!==false,
	uri: state => state.uri,
	conteneurEnChargement: state => state.conteneurEnChargement,
	question: state => state.question,
	séquence: state => state.séquence,
	question_type: state => state.question?.sous_type ?? TYPE.SEQ,
	résultats: state => state.tentative?.resultats?.map( (x) => x?.résultat ) ?? [],
	dev: state => state.dev,
	xray: state => state.user?.préférences["xray"] !== false,
};
