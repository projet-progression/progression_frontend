const USER = {
	INACTIF: "inactif",
	ACTIF : "actif",
	EN_ATTENTE_DE_VALIDATION: "en_attente_de_validation",
};

const ÉTAT = {
	DÉBUT: "début",
	RÉUSSI: "réussi",
	NON_RÉUSSI: "non_réussi"
};

const TYPE = {
	PROG: "prog",
	SYS: "sys",
	SEQ: "seq"
};

export {
	USER,
	ÉTAT,
	TYPE
};
