import { getData, postData, patchData, putData } from "@/services/request_services";
import axios from "axios";

//Ressources permises par les token d'authentification.
//Exclut l'accès aux ressources tokens et cles
const ressources_token_auth = { expiration: "+300",
	fingerprint: true };

const authentifierApi = async (urlAuth, identifiant, mdp, domaine) => {
	const urlTokens = (await getData(urlAuth, null, { identifiant: identifiant, password: mdp, domaine: domaine })).data.links.tokens;

	const data = await postData(urlTokens, null, "token", null, ressources_token_auth, { identifiant: identifiant, password: mdp, domaine: domaine });

	return construireToken( data.data );
};

const inscrireApi = async (urlInscription, identifiant, courriel, mdp) => {
	const data = await postData(urlInscription, null, "user", null, { username: identifiant, courriel: courriel, password: mdp } );

	return construireUser(data.data, data.included);
};

const getTokenIDApi = async (urlAuth, identifiant, clé, token_modèle = ressources_token_auth ) => {
	const data = ( await postData( urlAuth,
	                               null,
	                               "token",
	                               null,
	                               token_modèle,
	                               {
	                                  identifiant: identifiant,
	                                  key_name: clé
	                               } ) );
	return construireToken( data.data, data.included );
};

const getTokenRessourceApi = async (urlAuth, token, modèle_token ) => {
	const token_ressource = ( await postData( urlAuth,
	                                          null,
	                                          "token",
	                                          null,
	                                          modèle_token,
	                                          token?.jwt ) ).data;
	return token_ressource ? construireToken( token_ressource ): null;
};

const getConfigServeurApi = async (urlConfig, token, identifiant, clé) => {
	var data = null;

	if(token){
		data = await getData(urlConfig, null, token?.jwt);
	}
	else if( identifiant && clé ){
		data = await getData(urlConfig, null, { identifiant: identifiant, key_name: clé });
	}
	else {
		data = await getData(urlConfig);
	}

	return construireConfig(data.data);
};

const getUserApi = async (urlUser, token) => {
	const query = { include: "avancements" };
	const data = await getData( urlUser, query, token?.jwt );

	return construireUser( data.data, data.included );
};

const getUserAvecTentativesApi = async (urlUser, token) => {
	const query = { include: "avancements.tentatives" };
	const data = await getData( urlUser, query, token?.jwt );

	return construireUser( data.data, data.included );
};

const construireUser = ( data, included = null ) => {
	if(!data) return null;

	var user = data.attributes;
	user.préférences = data.attributes.préférences ? JSON.parse(data.attributes.préférences) : {};
	user.liens = data.links;
	user.liens.avancements = data.relationships.avancements.links.related;
	user.liens.clés = data.relationships.cles.links.related;
	user.avancements = {};
	var tentatives = {};
	if (included) {
		included.forEach((item) => {
			if (item.type == "avancement") {
				var avancement = item.attributes;
				avancement.liens = item.links;
				avancement.relations = item.relationships;
				user.avancements[item.id] = avancement;
			}
			else if (item.type == "tentative") {
				var tentative = item.attributes;
				tentative.liens = item.links;
				tentatives[item.id] = tentative;
			}
		});

		Object.keys(user.avancements).forEach((id) =>{
			var avancement = user.avancements[id];
			avancement.tentatives = [];
			avancement.relations?.tentatives?.data?.forEach((tentative) => {
				var id = tentative["id"];
				avancement.tentatives.push(tentatives[id]);
			});
		});

	}

	return user;
};

function construireToken(data) {
	if(!data) return null;

	var token = data.attributes;
	token.liens = data.links;

	return token;
}

const getQuestionApi = async (urlQuestion, token) => {
	const query = { include: "tests,ebauches,questions" };
	const data = await getData(urlQuestion, query, token?.jwt);
	var question = construireQuestion( data.data, data.included );

	return question;
};

function construireQuestion ( data, included ){
	var question = data.attributes;
	question.id = data.id;
	question.liens = data.links;
	question.tests = [];
	question.questions = {};
	question.ebauches = {};
	if (included) {
		included.forEach((item) => {
			if (item.type == "test") {
				var test = item.attributes;
				test.liens = item.links;
				question.tests.push(test);
			} else if (item.type == "ebauche") {
				var ebauche = item.attributes;
				ebauche.liens = item.links;
				question.ebauches[ebauche.langage] = ebauche;
			} else if (item.type == "question") {
				var sous_question = item.attributes;
				sous_question.liens = item.links;
				sous_question = item.attributes;
				question.questions[item.id] = sous_question;
			}
		});
	}
	return question;
}

const getAvancementApi = async (url, token, tokenRessources) => {
	const query = { include: "tentatives.commentaires,sauvegardes,avancements", tkres: tokenRessources };
	const data = await getData(url, query, token?.jwt);
	var avancement = construireAvancement(data.data, data.included);
	return avancement;
};

const getBanqueEtQuestionsApi = async (url, token) => {
	const query = {
		include: "questions,banques"
	};
	const data = await getData(url, query, token?.jwt);

	const banque = construireBanque(data.data, data.included);

	return {id: data.data.id, banque: banque};
};

const getBanquesApi = async (url, token) => {
	const data = await getData(url, null, token?.jwt);
	return construireBanques( data.data, data.included);
};

const postBanqueApi = async (params, token) => {
	const data = await postData(params.url, null, "banque", `${params.username}/${params.banque.url}`, {nom: params.banque.nom}, token?.jwt);
	const banque = construireBanque(data.data, data.included);

	return {id: data.id, banque: banque};
};

const patchUserApi = async (params, token, tokenRessources) => {
	const url = params.url;
	const user = params.user;
	const query = { tkres: tokenRessources };
	
	const data = await patchData(url, query, "user", user, token?.jwt);

	return construireUser( data.data, data.included );
};

const putAvancementApi = async (params, token) => {
	const query = { include: "tentatives.commentaires,sauvegardes" };
	const body = {
		extra: params.avancement.extra
	};
	const data = await putData(params.url, query, "avancement", body, token?.jwt);
	return construireAvancement(data.data, data.included);
};

const patchAvancementApi = async (params, token) => {
	const query = { include: "tentatives,sauvegardes,avancements" };

	const data = await patchData(params.url, query, "avancement", params.avancement, token?.jwt);
	var avancement = construireAvancement(data.data, data.included);
	return avancement;
};

const getTousAvancementsApi = async (url, token, tokenRessources) => {
	const query = { tkres: tokenRessources };
	const data = await getData(url, query, token?.jwt);
	let avancements = {};
	data.data.forEach((item) => {
		avancements[item.id] = construireAvancement(item, null);
	});
	return avancements;
};

const postCommentaireApi = async (params, token, tokenRessources) => {
	const body = params;
	const query = { tkres: tokenRessources };
	const data = await postData(params.url, query, "commentaire", null, body, token?.jwt);

	const commentaire = construireCommentaire(data.data);

	return commentaire;
};

const getTentativeApi = async (url, token, tokenRessources) => {
	const query = { include: "commentaires", tkres: tokenRessources };
	const data = await getData(url, query, token?.jwt);

	if (data.erreur) {
		console.log(data.erreur);
		return null;
	}

	return construireTentative(data.data, data.included);
};

const postTentative = async (params, token) => {
	const urlRequete = params.urlTentative;
	const query = { include: "resultats" };
	const body = { langage: params.tentative.langage, code: params.tentative.code };
	const data = await postData(urlRequete, query, "tentative", null, body, token?.jwt);

	if (data.erreur) {
		console.log(data.erreur);
		return null;
	}

	return construireTentative( data.data, data.included );
};

const postTentativeSys = async (params, token) => {
	const urlRequete = params.urlTentative;
	const query = { include: "resultats" };
	const body = { conteneur_id: params.tentative?.conteneur_id };
	const data = await postData(urlRequete, query, "tentative", null, body, token?.jwt);

	if (data.erreur) {
		return null;
	}

	return construireTentative( data.data, data.included );
};

const patchTentativeApi = async (params, token) => {
	const url = params.url;
	const tentative = params.tentative;
	
	const data = await patchData(url, null, tentative, token?.jwt);

	return construireTentative( data.data, data.included );
};

const postRésultat = async (params, token) => {
	const urlRequete = params.url;
	const body = { langage: params.tentative.langage, code: params.tentative.code, test: params.test, index: params.index };
	const data = await postData(urlRequete, null, "resultat", null, body, token?.jwt);

	if (data.erreur) {
		console.log(data.erreur);
		return null;
	}

	var résultat = data.data.attributes;
	résultat.liens = data.data.links;
	return résultat;
};

const postAuthKey = async (params, identifiant, password, domaine) =>
	await postData(params.url, null, "cle", null, params.clé, { identifiant: identifiant, password: password, domaine: domaine } )
		.then((data) => {
			return {
				nom: params.clé.nom,
				clé: data.data.attributes
			};
		});

const callbackGrade = async (cb_succes, uri, token) => {
	const url = cb_succes.url;
	const params = {
		uri: uri,
		ltik: cb_succes.ltik,
		s: cb_succes.s,
		token: token?.jwt
	};
	return axios.post(url, params, {ltik: cb_succes.ltik}, {conf: {headers: {"Content-Type": "application/json"}} ,params: { ltik: cb_succes.ltik } });
};

const postSauvegardeApi = async (params, token) => {
	const urlRequete = params.url;
	const body = { langage: params.langage, code: params.code };
	const data = await postData(urlRequete, null, "sauvegarde", null, body, token?.jwt);

	if (data.erreur) {
		throw data.erreur;
	}

	return construireSauvegarde(data.data);
};

const postUserApi = async (params, token) => {
	const url = params.url;
	const body = { préférences: JSON.stringify(params.préférences) };
	const data = await postData( url, null, "user", null, body, token?.jwt );

	if (data.erreur) {
		throw data.erreur;
	}

	return construireUser( data.data, data.included );

};

function construireCommentaire(data){
	if(!data) return null;

	var commentaire = data.attributes;
	commentaire.liens = data.links;

	return commentaire;
}

function construireAvancement(data, included = null) {
	if(!data) return null;

	var avancement = data.attributes;
	avancement.liens = data.links;
	avancement.liens.sauvegardes = data.relationships.sauvegardes?.links.related;
	avancement.liens.tentatives = data.relationships.tentatives?.links.related;
	avancement.tentatives = [];
	avancement.sauvegardes = {};
	avancement.avancements = {};
	if(avancement.extra) avancement.extra = JSON.parse( avancement.extra );
	if (included) {
		included.forEach((item) => {
			if (item.type == "tentative") {
				avancement.tentatives.unshift(construireTentative(item,included));
			} else if (item.type == "sauvegarde") {
				var sauvegarde = construireSauvegarde(item);
				avancement.sauvegardes[sauvegarde.langage] = sauvegarde;
			} else if (item.type == "avancement") {
				var sous_avancement = construireAvancement(item);
				avancement.avancements[item.id] = sous_avancement;
			}
		});
	}
	return avancement;
}

function construireBanque(data, included = null) {
	
	var banque = data.attributes;
	banque.id = data.id;
	banque.liens = data.links;
	banque.relations = data.relationships;

	banque.questions={};
	banque.banques=null;
	if (included) {
		banque.banques = [];
		included.forEach((item) => {
			if (item.type == "question") {
				var question = construireQuestion( item, included );
				banque.questions[question.id] = question;
			}
			if (item.type == "banque") {
				if(item.attributes.nom){
					var sous_banque = construireBanque( item );
					banque.banques.push(sous_banque);
				}
			}
		});
	}
	return banque;
}

function construireBanques(data, included = null) {
	var banques = [];
	if(data != null){
		data.forEach((item) => {
			banques.push( construireBanque( item, included ));
		});

		return banques;
	}
}

function construireSauvegarde(data) {
	if(!data) return null;

	var sauvegarde = data.attributes;
	var res = data.id.split("/");
	var langage = res[res.length - 1];

	sauvegarde.liens = data.links;
	sauvegarde.langage = langage;

	return sauvegarde;
}

function construireConfig(data) {
	if(!data) return null;

	var config = data.attributes.config;
	config.liens = data.links;

	return config;
}

function construireTentative(data, included = null) {
	if(!data) return null;

	var tentative;
	tentative = data.attributes;
	tentative.modifiée = false;
	tentative.liens = data.links;
	tentative.liens.commentaires = data.relationships.commentaires.links.related;
	tentative.liens.resultats = data.relationships.resultats.links.related;

	tentative.resultats = [];
	tentative.commentaires = [];
	if (included) {
		included.forEach((item) => {
			if (item.type == "commentaire" && item.links.tentative == tentative.liens.self ) {
				const commentaire = {
					...item.attributes,
					liens: item.links
				};
				tentative.commentaires.push(commentaire);
			}
			else if (item.type == "resultat" && item.links.tentative == tentative.liens.self) {
				const résultat = {
					...item.attributes,
					liens: item.links
				};
				tentative.resultats.push(résultat);
			}
		});
	}
	return tentative;
}

export {
	authentifierApi,
	inscrireApi,
	callbackGrade,
	getConfigServeurApi,
	getAvancementApi,
	getBanquesApi,
	getBanqueEtQuestionsApi,
	postBanqueApi,
	getQuestionApi,
	getTentativeApi,
	getTokenIDApi,
	getTokenRessourceApi,
	getTousAvancementsApi,
	getUserApi,
	getUserAvecTentativesApi,
	patchAvancementApi,
	putAvancementApi,
	patchTentativeApi,
	patchUserApi,
	postCommentaireApi,
	postRésultat,
	postSauvegardeApi,
	postTentative,
	postTentativeSys,
	postAuthKey,
	postUserApi,
};
