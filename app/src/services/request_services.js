import axios from "axios";

axios.defaults.withCredentials = true;

function conf(query, creds, config) {
	let conf = {
		params: query,
		...config
	};

	conf.headers = {};
	if(creds){
		if (typeof creds === "string"){
			conf.headers = { Authorization: "Bearer " + creds };
		}
		else if (creds.identifiant && "password" in creds ){
			conf.headers = { Authorization: "Basic " + btoa(`${creds.identifiant}:${creds.password}:${creds.domaine??""}`) };
		}
		else if (creds.identifiant && creds.key_name){
			conf.headers = { Authorization: "Key " + btoa(`${creds.identifiant}:${creds.key_name}`) };
		}
	}

	conf.headers["Content-Type"] = "application/vnd.api+json";
	return conf;
}

async function getData(url, query = null, creds = null, config = null) {
	return (await axios.get(url, conf(query, creds, config))).data;
}

async function postData(url, query = null, type = null, id = null, data = null, creds = null, config = null) {
	if(type){
		data = {
			data: {
				type: type,
				id: id,
				attributes: {
					...data
				}
			}
		};
	}
	return (await axios.post(url, data, conf(query, creds, config))).data;
}

async function putData(url, query = null, type = null, data = null, creds = null, config = null) {
	if(type){
		data = {
			data: {
				type: type,
				attributes: {
					...data
				}
			}
		};
	}
	return (await axios.put(url, data, conf(query, creds, config))).data;
}

async function patchData(url, query = null, type = null, data = null, creds = null, config = null) {
	if(type){
		data = {
			data: {
				type: type,
				attributes: {
					...data
				}
			}
		};
	}
	return (await axios.patch(url, data, conf(query, creds, config))).data;
}

export { getData, postData, putData, patchData };
