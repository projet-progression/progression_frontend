const API_URL = import.meta.env.VITE_API_URL;

export default {
	data() {
		return {
			user: null,
			occupations: [
				{
					étiquette: this.$t("profil.occupations.étudiant"),
					valeur : "étudiant"
				},{
					étiquette: this.$t("profil.occupations.enseignant"),
					valeur : "enseignant"
				},{
					étiquette: this.$t("profil.occupations.tuteur"),
					valeur : "tuteur"
				},{
					étiquette: this.$t("profil.occupations.autre"),
					valeur : "autre"
				},
			],
			règles: {
				url: valeur => this.validerUrl(valeur),
				taille_max: valeur => valeur.length <= 255,
				taille_bio_max: valeur => valeur.length <= 65535,
				pseudonyme: valeur => this.validerPseudonyme( valeur )
			},
			avatar: ""
		};
	},
	computed: {
		modifiable(){
			return this.$store.getters.username == this.user?.username;
		},
		store_user(){
			return this.$store.getters.user;
		}
	},
	created(){
		this.charger_user();
	},
	watch: {
		$route(to){
			if(to.name=="Profil") this.charger_user();
		}
	},
	methods: {
		async charger_user(){
			const username = this.$route.params.username || this.$store.getters.user.username;

			this.user = await this.$store.dispatch("obtenirUser", API_URL + "/user/" + username );

			this.avatar = this.user?.avatar;
		},
		validerUrl( valeur ){
			if(valeur.trim() === "") return true;
			try{
				const url = new URL( valeur );
				if( url.protocol == "http:" || url.protocol == "https:" ) return true;
			}catch(e){
				// rien à faire
			}
			return "Pas un URL valide";
		},
		validerPseudonyme( valeur ){
			return valeur.length>=3 || this.$t("profil.validation.pseudo_longueur");
		},
		onNomChangé(ev) {
			if((!ev || ev.key=="Enter") && this.$store.getters.user.nom != this.user.nom )
				this.$store.dispatch("mettreÀJourUser", {url: this.user.liens.self, user : { nom: this.user.nom }} );
		},
		onPrénomChangé(ev) {
			if((!ev || ev.key=="Enter") && this.$store.getters.user.prénom != this.user.prénom )
				this.$store.dispatch("mettreÀJourUser", {url: this.user.liens.self, user : { prénom: this.user.prénom }} );
		},
		onNomCompletChangé(ev) {
			if((!ev || ev.key=="Enter") && this.$store.getters.user.nom_complet != this.user.nom_complet )
				this.$store.dispatch("mettreÀJourUser", {url: this.user.liens.self, user : { nom_complet: this.user.nom_complet }} );
		},
		onPseudoChangé(ev) {
			if((!ev || ev.key=="Enter") && this.$store.getters.user.pseudonyme != this.user.pseudonyme ){
				if(this.validerPseudonyme( this.user.pseudonyme ) === true){
					this.$store.dispatch("mettreÀJourUser", {url: this.user.liens.self, user : { pseudonyme: this.user.pseudonyme }}).catch( () =>{
						this.$store.dispatch("setErreurs", {message : "Le pseudo est déjà utilisé" });
					});
				}
			}
		},
		onAvatarChangé(ev) {
			if((!ev || ev.key=="Enter") && this.$store.getters.user.avatar != this.avatar && this.validerUrl(this.avatar) === true ){
				this.user.avatar = this.avatar;
				this.$store.dispatch("mettreÀJourUser", {url: this.user.liens.self, user : { avatar: this.avatar }} );
			}
		},
		onBioChangé(ev) {
			if((!ev || ev.key=="Enter") && this.$store.getters.user.biographie != this.user.biographie ){
				this.$store.dispatch("mettreÀJourUser", {url: this.user.liens.self, user : { biographie: this.user.biographie }} );
			}
		},
		onOccupationChangé() {
			if( this.$store.getters.user.occupation != this.user.occupation ){
				this.$store.dispatch("mettreÀJourUser", {url: this.user.liens.self, user : { occupation: this.user.occupation }} );
			}
		}
	}
};
