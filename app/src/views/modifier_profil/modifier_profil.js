import { useMeta } from "vue-meta";
import defaultAvatar from "../../assets/avatar.png";

export default {
	setup() {
		useMeta({
			title: "Profil Utilisateur | Progression",
			htmlAttrs: { lang: "fr", amp: true }
		});
	},
	data() {
		return {
			nom: "",
			prenom: "",
			nom_complet: "",
			pseudo: "",
			email: "test@example.com",
			biographie: "",
			occupation: "",
			avatar: "",
			nouveau_mdp: "",
			confirmer_mdp: "",
			connaissances: "",
			profileImage: localStorage.getItem("profileImage") || defaultAvatar,
			dev: this.$store.getters.dev,
		};
	},
	computed: {
		username() {
			return this.$store.state.username;
		}
	},
	mounted: function () {
		this.simulerProfil();  //Fait en sorte que la fonction s'exécute quand la page load
	},
	methods: {
		simulerProfil() {
			fetch("http://ordralphabetix.dti.crosemont.quebec:9142/user/martine/profile", {
				method: "GET",
				headers: {
					"Content-Type": "application/json",
				}
			}).then(response => {
				if (!response.ok) {
					console.log("ERREUR");
				}
				return response.json();
			}).then(data => {
				console.log(data);
				this.nom = data.nom;
				this.prenom = data.prenom;
				this.nom_complet = data.nom_complet;
				this.pseudo = data.pseudo;
				this.biographie = data.biographie;
				this.occupation = data.occupation;
				this.avatar = data.avatar;
			})
				.catch(error => {
					console.error("Erreur", error);
					this.$toast.error("Erreur");
				});
		},
		allerVersHome() {
			this.allerVers("Home");
		},
		async validate() {
			const { valid } = await this.$refs.form_modifier_profil.validate();
			if (valid) {
				// Rediriger vers la page de profil
				this.allerVers("Profil");
			}
		},
		annuler() {
			this.allerVers("Profil");
		},
		supprimer_photo() {
			//Supprimer la photo de profil
			this.profileImage = defaultAvatar;
		},
		allerVers(vue, query = null) {
			this.$router.push({
				name: vue,
				query: query
			});
		},
		selectionFichier() {
			this.$refs.fileInput.click();
		},
		previewImage(event) {
			const file = event.target.files[0];
			if (file) {
				const reader = new FileReader();
				reader.onload = (e) => {
					this.profileImage = e.target.result;
					this.sauvegardeImgProfile(e.target.result);
				};
				reader.readAsDataURL(file);
			}
		},
		sauvegardeImgProfile(imageData) {
			localStorage.setItem("profileImage", imageData);
			this.profileImage = imageData;
		},
	}
};