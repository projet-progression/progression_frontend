import OngletsInformation from "@/components/question/onglets_information/onglets_information.vue";
import Enonce from "@/components/question/enonce/enonce.vue";
import EditeurCode from "@/components/question/editeur/editeur.vue";
import EditeurToolbar from "@/components/question/editeur/toolbar.vue";
import RetroactionTentative from "@/components/question/retroaction_tentative/retroaction_tentative.vue";
import Présentation from "@/components/question/présentation/présentation.vue";
import PanneauCommentaire from "@/components/question/commentaires/sidebar.vue";
import Partage from "@/components/question/partage/partage.vue";
import Avancement from "@/components/question/avancement/avancement.vue";
import BoutonSoumission from "@/components/question/bouton_soumission/boutonSoumission.vue";
import BoutonRéinitialiserTests from "@/components/question/bouton_réinitialiser_tests/bouton_réinitialiser_tests.vue";
import TTYShare from "@/components/question/ttyshare/ttyshare.vue";
import Diptyque from "@/components/diptyque/diptyque.vue";
import MenuSéquence from "@/components/question/séquence/menu_séquence.vue";
import TitreSéquence from "@/components/question/séquence/titre_séquence.vue";
import {TYPE} from "@/util/constantes.js";

export default {
	name: "Question",
	data() {
		return {
			créerCommentaire: false
		};
	},
	components: {
		OngletsInformation,
		Enonce,
		Avancement,
		EditeurCode,
		EditeurToolbar,
		RetroactionTentative,
		Partage,
		Présentation,
		BoutonSoumission,
		BoutonRéinitialiserTests,
		PanneauCommentaire,
		Diptyque,
		TTYShare,
		MenuSéquence,
		TitreSéquence,
	},
	computed: {
		user() {
			return this.$store.state.user;
		},
		question() {
			return this.$store.state.question;
		},
		question_type() {
			return this.$store.getters.question_type;
		},
		séquence() {
			return this.$store.state.séquence;
		},
		taille_menu_séquence(){
			return Math.min(50, Object.keys(this.séquence.questions).length * 10);
		},
		avancement() {
			return this.$store.state.avancement;
		},
		tentative() {
			return this.$store.state.tentative;
		},
		tentative_modifiée() {
			return this.$store.state.tentative?.modifiée != false;
		},
		resultats() {
			return this.$store.state.tentative?.resultats;
		},
		uri() {
			return this.$store.state.uri;
		},
		lang() {
			return this.$store.state.langageDéfaut;
		},
		démo() {
			return this.$store.state.démo;
		},
		thèmeSombre() {
			return this.$store.getters.thèmeSombre;
		},
		indicateursDeFonctionnalitéCommentaires() {
			return this.$store.getters.indicateursDeFonctionnalité("commentaires");
		},
		raccourcis() {
			return this.$store.state.raccourcis;
		},
		taillePanneauÉnoncé() {
			return this.$store.getters?.préférences?.disposition?.énoncé ?? 30;
		},
		tailleÉditeur() {
			return this.$store.getters?.préférences?.disposition?.éditeur ?? 60;
		},
		ligneCommentairesAffichés() {
			return this.$store.getters.ligneCommentairesAffichés;
		}
	},
	watch: {
		avancement: function () {
			if (this.$store.state.question?.sous_type == TYPE.SYS) {
				this.$store.dispatch("soumettreTentative", true);
			}
		},
		resultats: function () {
			if (this.resultats) {
				for (var index in this.resultats) {
					if (!this.resultats[index]?.résultat) {
						this.panneauTestsAffiché = true;
						break;
					}
				}
			}
		},
	},
	mounted() {
		this.$store.dispatch("réinitialiserErreurs");
		this.$store.dispatch("setErreurCallback", null);
		this.traiterParamètresURL(window.location.search);
	},
	provide() {
		return {
			avancement: this.avancement
		};
	},
	methods: {
		redimensionnéÉnoncé( taille ) {
			const disposition = this.$store.getters?.préférences?.disposition ?? {énoncé: 0};
			disposition.énoncé = taille;
			this.$store.dispatch("setDisposition", disposition);
		},
		redimensionnéÉditeur(taille) {
			const disposition = this.$store.getters?.préférences?.disposition ?? { éditeur: 0 };
			disposition.éditeur = taille;
			this.$store.dispatch( "setDisposition", disposition );
		},
		traiterParamètresURL(paramètres) {
			var urlParams = new URLSearchParams(paramètres);

			this.$store.dispatch("setUri", urlParams.get("uri"));
			this.$store.dispatch("setLangageDéfaut", urlParams.get("lang"));
			this.$store.dispatch("setDémo", urlParams.has("demo"));
			this.$store.dispatch("setCallbackSucces", urlParams.has("cb_succes") ? {url: urlParams.get("cb_succes"), ltik: urlParams.get("ltik"), s: urlParams.get("s")} : null);
		},
		onCommentaire(){
			this.créerCommentaire=!this.créerCommentaire;
		}
	},
};
