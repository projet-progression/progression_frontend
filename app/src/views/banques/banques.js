import Diptyque from "@/components/diptyque/diptyque.vue";
import Banque from "@/components/banques/banque/banque.vue";
import Questions from "@/components/banques/questions/questions.vue";
import NouvelleBanque from "@/components/banques/nouvelle_banque/nouvelle_banque.vue";
import {btoa_url} from "@/util/url.js";

export default {

	name: "Banques",
	data() {
		return {
			nouvelle_banque_affiché : false,
			nouvelle_banque : { nom: "", url: "" }
		};
	},
	components: {
		Diptyque,
		Banque,
		Questions,
		NouvelleBanque
	},
	created() {
		if ( !this.$store.getters.banques || this.$store.getters.banques.length == 0) {
			this.obtenirBanques();
		}
	},
	mounted() {
		this.traiterParamètresURL(this.$route.query);
	},

	computed: {
		banques() {
			return this.$store.getters.banques?.sort( (a,b) => a.nom?.localeCompare(b.nom) );
		},
		banque_sélectionnée() {
			return this.$store.getters.banqueSélectionnée;
		},
		petit_écran(){
			return window.innerWidth < 800;
		}
	},

	methods: {
		async obtenirBanques() {
			return await this.$store.dispatch("récupérerBanques", {
				url: this.$store.state.user.liens.banques,
			});
		},

		afficher_nouvelle_banque(){
			this.nouvelle_banque_affiché = !this.nouvelle_banque_affiché;
		},

		async ajouterBanque() {
			const banque = await this.$store.dispatch("ajouterUneBanque", {
				url: this.$store.state.user.liens.banques,
				banque: {
					nom: this.nouvelle_banque.nom,
					url: btoa_url(this.nouvelle_banque.url)
				},
				username: this.$store.state.user.username
			});

			if(banque) {
				this.nouvelle_banque.url = "";
				this.nouvelle_banque.nom = "";
				this.banqueValide = true;
				this.obtenirBanques();
			}
		},

		traiterParamètresURL(urlParams) {
			if(urlParams.url){
				this.nouvelle_banque.url = urlParams.url;
				this.nouvelle_banque.nom = urlParams.nom;
				this.nouvelle_banque_affiché = !this.nouvelle_banque_affiché;
			}
		}
	}
};
