import "codemirror/addon/fold/brace-fold";
import "codemirror/addon/fold/foldgutter";
import "codemirror/addon/fold/foldgutter.css";
import "codemirror/mode/clike/clike";
import "codemirror/mode/shell/shell";
import "codemirror/mode/python/python";
import "codemirror/mode/javascript/javascript";
import "codemirror/mode/rust/rust";
import "codemirror/mode/sql/sql";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/monokai.css";
import Codemirror from "codemirror-editor-vue3";

export default {
	name: "SolutionsPartagée",
	components : {
		Codemirror
	},
	data() {
		return {
			cmOptions: {
				mode: this.mode("python"),
				theme: this.$store.getters.thèmeSombre?"monokai":"default",
				lineNumbers: true,
				indentUnit: 4,
				gutters: ["CodeMirror-linenumbers"],
				font: "monospace",
				readOnly: "nocursor"
			},
			uri: this.$route.query.uri,
			lang: this.$route.query.lang,
		};
	},
	created() {
		this.$store.dispatch("récupérerSolutionsPartagées", {url: "test"});
	},
	computed :{
		solutions() {
			return this.$store.getters.solutionsPartagées;
		},
	},
	methods: {
		mode(langage) {
			if (langage === "java") {
				return "mode", "text/x-java";
			} else if (langage === "javascript") {
				return "mode", "javascript";
			} else if (langage === "kotlin") {
				return "mode", "text/x-kotlin";
			} else if (langage === "typescript") {
				return "mode", "text/typescript";
			} else if (langage === "python") {
				return "mode", "python";
			} else if (langage === "bash") {
				return "mode", "shell";
			} else if (langage === "rust") {
				return "mode", "text/rust";
			} else if (langage === "c") {
				return "mode", "text/x-csrc";
			} else if (langage === "c#") {
				return "mode", "text/x-csharp";
			} else if (langage === "sql") {
				return "mode", "text/x-sql";
			} else if (["cpp", "c++"].includes(langage)) {
				return "mode", "text/x-c++src";
			} else {
				return "mode", langage;
			}
		},
		timestampVersDate: function (timestamp) {
			return new Date(timestamp * 1000).toLocaleDateString("fr-CA");
		}
	},
};
