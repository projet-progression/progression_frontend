import routes from "../src/routes.js";
import services from "../src/services.js";

import {
	describe,
	expect,
	test,
	vi
} from "vitest";

vi.mock("services");

describe("Grade", () => {
	services.récupérerScoreProgression = vi.fn((uri, token) => {
		if (uri == "uri" && token == "un token ressource") return 42;
		if (uri == "uri" && token == "un token invalide") throw {statusCode: 401};
		throw new Error("erreur");
	});

	services.envoyerNote = vi.fn( (idToken, {score, token}) => {
		expect(idToken).toEqual({id: "token"});
		expect(score).toEqual(42);
		expect(token).toEqual("un token ressource");
	});

	test("étant donné l'URI d'un exercice réussi, lorsqu'on soumet le score, il est envoyé" , async () => {
		const res = await routes.grade( {
			uri: "uri",
			ltik: "ltik",
			s: "KMsy_7_IoAa9Ky6WHlwOE0feRPzxyb3W8GQjfVCaAmU",
			token: "un token ressource"
		}, "ltik", {id: "token"});
		expect(res).toEqual(true);
	});

	test("étant donné l'URI d'un exercice réussi, lorsqu'on soumet le score d'un autre exercice, il n'est pas envoyé et on reçoit une erreur de Permissions ", async () => {
		const res = await routes.grade({
			uri: "autre uri",
			ltik: "ltik",
			s: "KMsy_7_IoAa9Ky6WHlwOE0feRPzxyb3W8GQjfVCaAmU",
			token: "un token ressource"
		}, "ltik", { id: "token" });

		expect(res).toEqual( {message: "La signature ne correspond pas aux paramètres reçus.", code: 403});
		expect(services.récupérerScoreProgression).not.toHaveBeenCalled();
		expect(services.envoyerNote).not.toHaveBeenCalled();
	});

	test("étant donné un token invalide, lorsqu'on soumet le score, il n'est pas envoyé et on reçoit une erreur de Connexion", async () => {
		const res = await routes.grade({
			uri: "uri",
			ltik: "ltik",
			s: "KMsy_7_IoAa9Ky6WHlwOE0feRPzxyb3W8GQjfVCaAmU",
			token: "un token invalide"
		}, "ltik", { id: "token" });

		expect(res).toEqual( {message: "Le token ne permet pas de récupérer la note.", code: 403});
		expect(services.envoyerNote).not.toHaveBeenCalled();
	});
	
});

