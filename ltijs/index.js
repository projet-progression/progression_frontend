import dotenv from "dotenv";
import path from "path";
import debug from "debug";
import {Provider as lti} from "ltijs";
import mongoose from "mongoose";
import routes from "./src/routes.js";
import utils from "./src/utils.js";
import services from "./src/services.js";
import consolidate from "consolidate";
import jwt_encode from "jwt-encode";
import jwt_decode from "jwt-decode";
import Router from "express";

dotenv.config();
const router = Router();
const provMainDebug = debug("provider:main");

const platformContact = new mongoose.Schema({
	platformUrl: String,
	name: String,
	email: String
});

const secret = process.env.LTI_KEY;

try {
	mongoose.model("platformContact", platformContact);
} catch (err) {
	provMainDebug("Model platformContact already registered. Continuing");
}

lti.setup(
	process.env.LTI_KEY,
	{
		url: `mongodb://${process.env.DB_HOST}/${process.env.DB_NAME}?authSource=admin`,
		connection: { user: process.env.DB_USER, pass: process.env.DB_PASS },
	},
	{
		appRoute: "/lti/",
		loginRoute: "/lti/login",
		keysetRoute: "/lti/keys",
		staticPath: path.join("./public"),
		cookies: {
			secure: false,
			sameSite: "",
		},
		devMode: false,
		dynRegRoute: "/lti/register",
		dynReg:  {
			url: process.env.URL_BASE,
			name: "Progression",
			logo: `${process.env.URL_BASE}/favicon.ico`,
			description: "Un exerciseur conçu pour aider l'apprentissage de la programmation par la pratique",
			autoActivate: true
		},
		ltiaas: true
	},
);

// When receiving successful LTI launch redirects to app
lti.onConnect(async (idToken, req, res) => {
	provMainDebug("onConnect");

	const userId = `${idToken.platformId}/${idToken.user}`;
	provMainDebug(`userId : ${userId}`);
	const uri = utils.btoa_url(res.locals.context.custom.uri || res.locals.context.custom.src);
	provMainDebug(`uri : ${uri}`);
	const lang = res.locals.context.custom.lang;
	provMainDebug(`lang : ${lang}`);

	if(res.locals.context.roles == "http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor"){
		const resLocalsToken = res.locals.token;
		const membres = await services.récupérerMembres( resLocalsToken );
		const scores = await services.récupérerScores( resLocalsToken );

		Object.values(membres).forEach( membre => {
			membre["score"] = scores[membre.user_id];

			const token_décodé = membre?.score?.comment ? jwt_decode( membre.score.comment ) : null;

			if(membre["score"]){
				membre["score"]["timestamp"] = new Date(scores[membre.user_id]["timestamp"]).toLocaleString("fr-CA");
				membre["score"]["réussite"] = scores[membre.user_id]["resultScore"]==100?"Réussi":"Débuté";
				membre["tkres"] = membre["score"]["comment"];
				membre["url"] = token_décodé?.data?.url ?? `${process.env.URL_BASE}/question?uri=${uri}&lang=${lang}`;
			}
		});

		res.render("suivi", { membres: Object.values( membres ), url: `${process.env.URL_BASE}/question?uri=${uri}&lang=${lang}`});
		res.status(200);
	}
	else{

		const signature = jwt_encode(
			{
				uri: uri,
				ltik: res.locals.ltik,
			}, secret ).split(".")[2];

		const query = {
			uri: uri,
			lang: lang,
			s: signature,
			cb_succes: `${process.env.URL_BASE}/lti/grade`,
		};

		provMainDebug(`Redirection vers : ${process.env.URL_BASE}/question`);
		lti.redirect(res, `${process.env.URL_BASE}/question`, {
			query: query
		});
	}
});

lti.onDynamicRegistration(async (req, res) => {
	try {
		if (!req.query.openid_configuration) {
			return res.status(400).send({ status: 400, error: "Bad Request", details: { message: "Missing parameter: \"openid_configuration\"." } });
		}
		if (!req.body.email || !req.body.nom) {
			return res.render("inscription", {
				openid_configuration: req.query.openid_configuration,
				registration_token: req.query.registration_token
			});
		}
		const message = await lti.DynamicRegistration.register(
			req.query.openid_configuration,
			req.query.registration_token);

		res.setHeader("Content-type", "text/html");
		return res.send(message);
	} catch (err) {
		if (err.message === "PLATFORM_ALREADY_REGISTERED") {
			return res.status(403).send({ status: 403, error: "Forbidden", details: { message: "Platform already registered." } });
		}
		return res.status(500).send({ status: 500, error: "Internal Server Error", details: { message: err.message } });
	}
});

// Sur le clic du bouton «Sélectionner le contenu»
lti.onDeepLinking(async (token, req, res) => {
	// Redirige vers la banque de questions.
	return lti.redirect(res, `${process.env.URL_BASE}/banques?embed=true`);
});

// Sur la sélection du contenu
lti.app.post("/lti/deeplink", async (req, res) => {
	const resource = req.body;
	const items = [
		{
			type: "ltiResourceLink",
			title: resource.title,
			text: resource.description,
			custom: {
				src: resource.src,
			}
		}
	];

	const form = await lti.DeepLinking.createDeepLinkingForm(res.locals.token, items, { message: "La ressource a été correctement sélectionnée." });

	return res.send(form);
});

router.post("/lti/grade", async (req, res) => {
	const résultat = await routes.grade(req.body, res.locals.ltik, res.locals.token);
	if(résultat === true){
		res.status(201).send();
	}
	else{
		provMainDebug(résultat.message);
		res.status(résultat.code).send(résultat.message);
	}
});

router.get("*", routes.notfound);

lti.app.use(router);

const setup = async () => {
	const app = lti.app;
	app.engine("html", consolidate.mustache);
	app.set("view engine", "html");
	app.set("views", "./templates");

	await lti.deploy({ port: process.env.PORT });
};

setup();

export default {lti};
