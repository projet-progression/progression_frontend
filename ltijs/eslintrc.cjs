module.exports = {
    "env": {
		"node": true,
		"es2021": true
    },
    "extends": [
		"eslint:recommended",
    ],
	"ignorePatterns": [
		"/dist/"
	],
    "parserOptions": {
		"ecmaVersion": "latest",
		"sourceType": "module"
    },
    "plugins": [
		"smarter-tabs",
    ],
    "rules": {
		//JS
		"indent": [
			"error",
			"tab"
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		],
		"no-mixed-spaces-and-tabs": [
			"error",
			"smart-tabs"
		],
		"no-unused-vars": ["error", { "argsIgnorePattern": "_" }],
		"smarter-tabs/smarter-tabs": "error",
	}
};
