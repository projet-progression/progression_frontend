const btoa_url = (s) =>
	btoa(unescape(encodeURIComponent(s)))
	      .replace(/\//g, "_")
	      .replace(/\+/g, "-")
		.replace(/=/g, "");

export default {
	btoa_url
};
