import path from "path";
import debug from "debug";
import jwt_encode from "jwt-encode";
import services from "./services.js";

const provMainDebug = debug("provider:main");
const secret = process.env.LTI_KEY;

async function grade(body, ltik, idToken) {
	provMainDebug("/lti/grade");

	const uri = body.uri;
	const signature = body.s;
	const token = body.token;

	if(!ltik || !uri || !signature || !token){
		return {message: "Les paramètres uri, s et token sont obligatoires.", code: 400};
	}

	const signature_token = jwt_encode(
		{
			uri: uri,
			ltik: ltik,
		}, secret ).split(".")[2];

	if(signature !== signature_token){
		return {message: "La signature ne correspond pas aux paramètres reçus.", code: 403};
	}
	

	var score;
	try{
		score = await services.récupérerScoreProgression(uri, token);
	}
	catch(e){
		if(e?.statusCode == 401){
			return {message: "Le token ne permet pas de récupérer la note.", code: 403};
		}
		else{
			return {message: "Impossible de récupérer la note: " + e.message, code: 502};
		}
	}

	try{
		await services.envoyerNote(idToken, {score, token} );
	}
	catch(e){
		return {message: "Impossible de sauvegarder la note: " + e.message, code: 502};
	}

	return true;
}

function notfound(req, res) {
	return res.sendFile(path.join(__dirname, "../public/404.html"));
}

export default { grade, notfound };
