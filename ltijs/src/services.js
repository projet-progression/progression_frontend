import dotenv from "dotenv";
import jwt_decode from "jwt-decode";
import axios from "axios";
import debug from "debug";
import {Provider as lti} from "ltijs";

dotenv.config();
const provMainDebug = debug("provider:main");

const récupérerMembres = async function(token){
	const context = await lti.NamesAndRoles.getMembers(token, { resourceLinkId: true, role: " http://purl.imsglobal.org/vocab/lis/v2/membership#Learner "});
	provMainDebug(context.members.length + " membres récupérés");

	const membres = new Object();

	context.members.forEach( (membre) => {
		membres[membre.user_id] = membre;
	} );
	return membres;
};

const récupérerScores = async function(token){
	const scores = await lti.Grade.getScores(token, token.platformContext.endpoint.lineitem);
	provMainDebug(scores.scores.length + " scores récupérés");

	const userScore = new Object();
	scores.scores.forEach( (score) => {
		userScore[score.userId] = score;
	});

	return userScore;
};


const récupérerScoreProgression = async function (uri, token) {
	provMainDebug("Requête : " + process.env.API_URL + "/avancement");
	provMainDebug("Params :  uri : " + uri);
	const username = jwt_decode(token).username;

	const config = {
		headers: {
			"Content-Type": "application/vnd.api+json",
		},
		params: {
			"tkres": token
		}
	};

	const requête = `${process.env.VITE_API_URL}/avancement/${username}/${uri}`;

	const res = await axios.get(requête, config);
	return res.data.data.attributes.état == "réussi" ? 100 : 0;
};

const envoyerNote = async function(idToken, {score, token}) {
	// Note
	const gradeObj = {
		userId: idToken.user,
		scoreGiven: score,
		scoreMaximum: 100,
		comment: token,
		activityProgress: "Completed",
		gradingProgress: "FullyGraded",
	};

	// Selecting linetItem ID
	// Attempting to retrieve it from idtoken
	let lineItemId = idToken.platformContext.endpoint.lineitem;

	if (!lineItemId) {
		const response = await lti.Grade.getLineItems(idToken, { resourceLinkId: true });
		const lineItems = response.lineItems;
		provMainDebug("lineItem: " + idToken.platformContext.endpoint.lineitem);
		provMainDebug(lineItems);
		if (lineItems.length === 0) {
			// Creating line item if there is none
			provMainDebug("Création d'un item de notation");
			const newLineItem = {
				scoreMaximum: 100,
				label: "Grade",
				tag: "grade",
				resourceLinkId: idToken.contextId,
			};
			const lineItem = await lti.Grade.createLineItem(idToken, newLineItem);
			lineItemId = lineItem.id;
		} else lineItemId = lineItems[0].id;
	}

	// Envoie de la note
	return await lti.Grade.submitScore(idToken, lineItemId, gradeObj);
};

export default {
	récupérerMembres,
	récupérerScoreProgression,
	récupérerScores,
	envoyerNote
};
